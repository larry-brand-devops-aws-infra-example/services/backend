# Backend docker image

## How to run:
Execute command `docker-compose up` with [docker-compose-file](https://gitlab.com/larry-brand-devops-aws-infra-example/ci-cd/kubernetes/-/blob/master/local/docker-compose.yml)

or execute command

`$docker run --name example-app-backend -itd -p 8081:8080 larrybrand/devops-aws-infra-example-backend`

## How to run in k8s cluster:
Open port 8080 and pass environment variables "MONGODB_HOST=example-app-database.default.svc.cluster.local"
