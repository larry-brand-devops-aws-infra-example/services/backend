package com.hello.application.controller;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
public class Orders {
    @Id
    private String id;

    private String email;

    private List<String> callnumbers;

}
