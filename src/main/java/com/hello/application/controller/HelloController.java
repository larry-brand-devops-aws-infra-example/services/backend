package com.hello.application.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/backend-api")
@Slf4j
public class HelloController {

    @Autowired
    private OrdersRepository repository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity getHelloMessage() {
        return ResponseEntity.ok()
                             .body("Hello from backend!!!");
    }

    @RequestMapping(value = "/getOrders", method = RequestMethod.GET)
    public ResponseEntity getOrders() {
        StringBuilder sb = new StringBuilder();
        for (Orders orders : repository.findAll()) {
            sb.append(orders);
        }
        log.info(sb.toString());
        return ResponseEntity.ok()
                .body(new SimpleDateFormat("HH:mm:ss").format(new Date())
                        + " getOrders: " + sb );
    }
}
